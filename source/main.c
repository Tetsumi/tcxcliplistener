/*
  tcxcliplistener
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2019 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xfixes.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define E_PRINT(fmt, args...) fprintf(stderr, "[ERROR][%s:%d:%s]: " fmt, \
				      __FILE__, __LINE__, __func__, ##args)


Display *g_dpy;
Atom     g_cbAtom;
Atom     g_utf8Atom;
Atom     g_incrAtom;
Window   g_cbWindow;
Window   g_rootWindow;
int      g_xfixesSelNotify;
Atom     g_reqProp;

int getTextClipboard(Atom target, char **str)
{
	*str = NULL;
	
	XConvertSelection(g_dpy,
			  g_cbAtom,
			  target,
			  g_reqProp,
			  g_cbWindow,
			  CurrentTime);	

	XEvent evt;
	
	while(true)
	{
		XNextEvent(g_dpy, &evt);
		
		if (SelectionNotify != evt.type)
			continue;
		
	        if (None == evt.xselection.property)
			return 1;

		break;
	}

	Atom typeRet;
	int format;
	unsigned long numItems;
	unsigned long bytesRemaining;
	unsigned char *propReturn;
	
	XGetWindowProperty(g_dpy,
			   g_cbWindow,
			   g_reqProp,
			   0,
			   0,
			   False,
			   AnyPropertyType,
			   &typeRet,
			   &format,
			   &numItems,
			   &bytesRemaining,
			   &propReturn);
	XFree(propReturn);

	if (g_incrAtom == typeRet)
		return 2;

	XGetWindowProperty(g_dpy,
			   g_cbWindow,
			   g_reqProp,
			   0,
			   bytesRemaining,
			   False,
			   AnyPropertyType,
			   &typeRet,
			   &format,
			   &numItems,
			   &numItems,
			   &propReturn);

	*str = propReturn;
	
	return 0;
}

int onNewOwner(XFixesSelectionNotifyEvent *sne)
{
	char *name = NULL;
	char *parentName = NULL;	
	Window parent;
	Window  root;
	Window *children = NULL;
	unsigned int numChildren;
	char *data;

	getTextClipboard(g_utf8Atom, &data);
	
	if (!data)
		getTextClipboard(XA_STRING, &data);
	
	XFetchName(g_dpy, sne->owner, &name);
	XQueryTree(g_dpy, sne->owner, &root, &parent, &children, &numChildren);
	XFetchName(g_dpy, parent, &parentName);
	printf("Window:      0x%08x %s\n"
	       "Parent:      0x%08x %s\n"
	       "Timestamps:  %10lu %10lu\n",
	       sne->owner,
	       name,
	       parent,
	       parentName,
	       sne->timestamp,
	       sne->selection_timestamp);

        if(data)
	{
		printf("---DATA START---\n%s\n---DATA END---\n\n", data);
		XFree(data);
	}
	else
		puts("*** Could not retrieve data ***\n");
		

	XFree(name);
	XFree(parentName);
	XFree(children);

	return 0;
}

int initialize(void)
{
	g_dpy = XOpenDisplay(NULL);
	
	if (!g_dpy)
	{
		E_PRINT("Couldn't not open display.\n");
		
		return 1;
	}
	
	int majorVersion = 0;
	int minorVersion = 0;
	int eventBase = 0;
	int errorBase = 0;
	
	XFixesQueryVersion(g_dpy, &majorVersion, &minorVersion);
	XFixesQueryExtension(g_dpy, &eventBase, &errorBase);
	
	if (0 == majorVersion)
	{
		E_PRINT("XFixes not supported.\n");
		XCloseDisplay(g_dpy);
		
		return 1;
	}

	g_xfixesSelNotify = eventBase + XFixesSelectionNotify;
	g_utf8Atom = XInternAtom(g_dpy, "UTF8_STRING", False);
	g_cbAtom = XInternAtom(g_dpy, "CLIPBOARD", False);
	g_reqProp = XInternAtom(g_dpy, "CBDATA", False);
	g_incrAtom = XInternAtom(g_dpy, "INCR", False);
	g_rootWindow = DefaultRootWindow(g_dpy);
	
	XFixesSelectSelectionInput(g_dpy,
				   g_rootWindow,
				   g_cbAtom,
				   XFixesSetSelectionOwnerNotifyMask);

	g_cbWindow = XCreateSimpleWindow(g_dpy,
					 g_rootWindow,
					 -10,
					 -10,
					 1,
					 1,
					 0,
					 0,
					 0);

	if (!g_cbWindow)
	{
		E_PRINT("Couldn't create window.\n");
		
		return 1;
	}
	
	XSelectInput(g_dpy, g_cbWindow, SelectionNotify);	
	
	return 0;
}

int finalize(void)
{
	XCloseDisplay(g_dpy);
}

int main(void)
{	
	if(initialize())
		return EXIT_FAILURE;
    
	XEvent evt;
	
	while(true)
	{	    
		XNextEvent(g_dpy, &evt);
		if (g_xfixesSelNotify == evt.type)
			onNewOwner((XFixesSelectionNotifyEvent*)&evt);
	}
	
	finalize();

	return EXIT_SUCCESS;
}
