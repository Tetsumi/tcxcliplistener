#!/bin/bash

nd='-DNDEBUG'

if [ "${1+defined}" ] && [ $1 = "-d" ]; then
    nd="-g"
fi

gcc -lX11 -lXfixes -O3 ${nd} ./source/*.c -o ./bin/tcxcliplistener
