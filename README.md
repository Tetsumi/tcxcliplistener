  tcxcliplistener
=========

Little demo program to show how to monitor the X Window clipboard by using Xlib.

  How to use
==============

Run `tcxcliplistener`.

  How to compile
==================

Run `make.bash`. Use the `-d` option to enable assertions and debug informations.

or compile with the following options

`-lX11 -lXfixes -O3 ./source/*.c -o ./bin/tcxcliplistener`

  Dependencies
================

- GNU C11
- POSIX
- Xlib

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)
